$(document).ready(function() {
  loadMap();
});

function loadMap() {
  Papa.parse("locations.csv", {
    download: true,
    complete: function(results) {
      if (results.data && results.data.length) {
        createMap(results.data);
      }
    }
  });
}

function createMap(locations) {
  if(locations[1] && locations[1][2] && locations[1][3] && locations[1][4]) {
    var zoom = parseInt(locations[1][2]);
    var lat = parseFloat(locations[1][3]);
    var lng = parseFloat(locations[1][4]);
  }
  else {
    var zoom = 7;
    var lat = 54.49388164190735;
    var lng = -3.81292473517205;
  }
  var mapOptions = {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoom: zoom,
    center: new google.maps.LatLng(lat, lng),
    streetViewControl: false
  };
  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  map.potential = [];
  map.showing_potential = true;

  map.infowindow = new google.maps.InfoWindow();
  addHidePotentialButton(map);
  addLegend(map);
  addLocations(map, locations);
  getRoutes(map);
}

function addLocations(map, locations) {
  for (var i=0; i<locations.length; i++) {
    if(i === 0) continue;
    var location = locations[i];
    if (location[0]) {
      addLocationButton(
        map, 
        location[0], 
        location[1], 
        parseInt(location[2]),
        parseFloat(location[3]), 
        parseFloat(location[4]),
        i === 1
      );
    }
  }
}

function getRoutes(map) {
  Papa.parse("routes.csv", {
    download: true,
    complete: function(results) {
      if (results.data && results.data.length) {
        addRoutes(map, results.data);
      }
    }
  });
}

function addRoutes(map, routes) {
  for (var i=0; i<routes.length; i++) {
    if(i === 0) continue;
    var route = routes[i];
    if (route[0]) {
      loadGPXFileIntoGoogleMap(
        map, 
        "/gpx/"+route[0]+".gpx",
        route[2] ? true : false,
        route[3],
        route[4],
        route[5],
      );
    }
  }
}

function loadGPXFileIntoGoogleMap(map, filename, potential, title, time, difficulty) {
  $.ajax({url: filename,
    dataType: "xml",
    success: function(data) {
      var parser = new GPXParser(data, map);
      if (potential) {
        parser.setTrackColour("#FF0000");
        parser.setTrackOpacity(0.4);
      }
      parser.setTrackWidth(4);                // Set the track line width
      parser.setMinTrackPointDelta(0.001);    // Set the minimum distance between track points
      parser.addTrackpointsToMap();           // Add the trackpoints
      parser.addRoutepointsToMap();           // Add the routepoints
      // parser.addWaypointsToMap();             // Add the waypoints
      if (potential) {
        map.potential.push(parser);
      }
      for (var i=0; i<parser.polylines.length; i++) {
        var polyline = parser.polylines[i];
        addRouteLabel(map, polyline, title, time, difficulty, filename);
      }
    }
  });
}

function addHidePotentialButton(map) {
  var control = createControl("Hide Potential", "Click to hide potential routes");

  control.ui.addEventListener("click", () => {
    if (map.showing_potential) {
      for (var i=0; i<map.potential.length; i++) {
        map.potential[i].hidePolylines();
      }
      control.text.innerHTML = "Show Potential";
      map.showing_potential = false;
    }
    else {
      for (var i=0; i<map.potential.length; i++) {
        map.potential[i].showPolylines();
      }
      control.text.innerHTML = "Hide Potential";
      map.showing_potential = true;
    }
  });

  control.ui.style.marginBottom = "24px";
  map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(control.div);
}

function addLocationButton(map, text, title, zoom, lat, lng, bold) {
  var control = createControl(text, title);
  control.div.setAttribute("class", "location_button");

  control.ui.addEventListener("click", () => {
    map.setZoom(zoom);
    map.setCenter(new google.maps.LatLng(lat, lng)); 
  });
  control.ui.style.marginRight = "10px";
  if (bold) {
    control.text.style.fontWeight = "bold";
  }
  map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(control.div);
}

function addLegend(map) {
  var legendDiv = document.createElement("div");
  legendDiv.setAttribute("id", "key");
  var legendUI = document.createElement("div");
  addBoxStyles(legendUI);
  legendUI.style.textAlign = "left";
  legendUI.style.marginLeft = "10px";
  legendUI.style.paddingLeft = "15px";
  legendUI.style.paddingRight = "15px";
  legendDiv.appendChild(legendUI);
  var legendTitle = document.createElement("h3");
  addTextStyles(legendTitle);
  legendTitle.innerHTML = 'Key';
  legendUI.appendChild(legendTitle);
  addLegendItem(legendUI, "Completed", "0288D1", "0.8");
  addLegendItem(legendUI, "Potential", "FF0000", "0.4");
  map.controls[google.maps.ControlPosition.LEFT_CENTER].push(legendDiv);
}

function createControl(text, title) {
  var controlDiv = document.createElement("div");
  // Set CSS for the control border.
  var controlUI = document.createElement("div");
  addBoxStyles(controlUI);
  controlUI.style.cursor = "pointer";
  controlUI.style.marginBottom = "6px";
  controlUI.style.textAlign = "center";
  controlUI.title = title;
  controlDiv.appendChild(controlUI);
  // Set CSS for the control interior.
  var controlText = document.createElement("div");
  addTextStyles(controlText);
  controlText.style.lineHeight = "36px";
  controlText.style.paddingLeft = "15px";
  controlText.style.paddingRight = "15px";
  controlText.innerHTML = text;
  controlUI.appendChild(controlText);

  return {div: controlDiv, ui: controlUI, text: controlText};
}

function addBoxStyles(element) {
  element.style.backgroundColor = "#fff";
  element.style.border = "2px solid #fff";
  element.style.borderRadius = "2px";
  element.style.boxShadow = "rgb(0 0 0 / 30%) 0px 1px 4px -1px";
}

function addTextStyles(element) {
  element.style.color = "rgb(25,25,25)";
  element.style.fontFamily = "Roboto,Arial,sans-serif";
  element.style.fontSize = "16px";
}

function addColourBlockStyles(element) {
  element.style.height = "10px";
  element.style.width = "10px";
  element.style.display = "block";
  element.style.float = "left";
  element.style.marginTop = "3px";
  element.style.marginRight = "8px";
  element.style.borderRadius = "2px";
}

function addLegendItem(legendUI, title, colour, opacity) {
  var legendItem = document.createElement("div");
  var legendItemTitle = document.createElement("p");
  addTextStyles(legendItemTitle);
  legendItemTitle.style.fontSize = "14px";
  legendItemTitle.innerHTML = title;
  var legendItemColour = document.createElement("span");
  addColourBlockStyles(legendItemColour);
  legendItemColour.style.backgroundColor = "#"+colour;
  legendItemColour.style.opacity = opacity;
  legendItemTitle.appendChild(legendItemColour);
  legendItem.appendChild(legendItemTitle);
  legendUI.appendChild(legendItem);
}

function createInfo(title, time, difficulty, filename) {
  var infoDiv = document.createElement("div");
  var infoUI = document.createElement("div");
  infoUI.style.paddingRight = "3px";
  infoUI.style.paddingLeft = "3px";
  infoUI.style.marginBottom = "-0.8em";
  infoDiv.appendChild(infoUI);
  var infoTitle = document.createElement("h3");
  infoTitle.innerHTML = 'Route Info';
  infoTitle.style.marginTop = "5px";
  infoTitle.style.fontSize = "14px";
  infoUI.appendChild(infoTitle);
  var routeTitle = document.createElement("p");
  infoUI.appendChild(routeTitle);
  routeTitle.innerHTML = title;
  routeTitle.style.fontSize = "14px";
  if (time) {
    var routeTime = document.createElement("p");
    routeTime.innerHTML = time ? time+' hrs' : '';
    routeTime.style.fontSize = "14px";
    infoUI.appendChild(routeTime);
  }
  if (difficulty) {
    var routeDiff = document.createElement("p");
    routeDiff.innerHTML = difficulty;
    routeDiff.style.fontSize = "14px";
    infoUI.appendChild(routeDiff);
  }
  var gpxDownload = document.createElement("p");
  var gpxDownloadLink = document.createElement("a");
  gpxDownloadLink.setAttribute('href', filename);
  gpxDownloadLink.setAttribute('download', '');
  var downloadSVG = document.getElementById("download");
  gpxDownloadLink.innerHTML = downloadSVG.innerHTML+' GPX';
  gpxDownloadLink.style.fontSize = "12px";
  gpxDownloadLink.style.background = "#0288D1";
  gpxDownloadLink.style.color = "#fff";
  gpxDownloadLink.style.borderRadius = "3px";
  gpxDownloadLink.style.display = "inline-block";
  gpxDownloadLink.style.padding = "5px 10px";
  gpxDownloadLink.style.textDecoration = "none";
  gpxDownload.appendChild(gpxDownloadLink);
  infoUI.appendChild(gpxDownload);
  return infoDiv.innerHTML;
}

function addRouteLabel(map, polyline, title, time, difficulty, filename) {
  google.maps.event.addListener(polyline, 'click', function(event) {
    map.infowindow.close();
    map.infowindow.setContent(createInfo(title, time, difficulty, filename));
    map.infowindow.setPosition(event.latLng);
    map.infowindow.open(map);
  });
  google.maps.event.addListener(polyline, 'mouseover', function(latlng) {
      polyline.originalStrokeColor = polyline.strokeColor;
      polyline.originalStrokeOpacity = polyline.strokeOpacity;
      polyline.setOptions({
        strokeColor: "#00FFAA",
        strokeOpacity: 1,
        zIndex: 2
      });
  });
  google.maps.event.addListener(polyline, 'mouseout', function(latlng) {
      polyline.setOptions({
        strokeColor: polyline.originalStrokeColor,
        strokeOpacity: polyline.originalStrokeOpacity,
        zIndex: 1
      });
  });
}